public class AngestellterTest
{
 //------------Hauptprogramm---------------
 public static void main(String[] args)
 {
 Angestellter ang1 = new Angestellter();
 Angestellter ang2 = new Angestellter();


 //Setzen der Attribute
 ang1.setName("Anthony");
 ang1.setGehalt(600000);
 ang2.setName("Svenja");
 ang2.setGehalt(60000000);
 //Bildschirmausgaben
 System.out.println("Name: " + ang1.getName());
 System.out.println("Gehalt: " + ang1.getGehalt() + " Euro");
 System.out.println("\nName: " + ang2.getName());
 System.out.println("Gehalt: " + ang2.getGehalt() + " Euro");
 }//main
}//class