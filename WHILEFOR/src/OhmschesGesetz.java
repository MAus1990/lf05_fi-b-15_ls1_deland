import java.util.Scanner;

class OhmschesGesetz {
  public static void main(String[] args) {

    char operator;
    Double number1, number2, result;

 
    Scanner input = new Scanner(System.in);

    
    System.out.println("Welche gr��e soll berechnet werden : R,U,I");
    operator = input.next().charAt(0);
    if(operator != 'R' || operator != 'U' || operator != 'I')
    {
    	System.out.println("Falsche Eingabe!");
    }

    System.out.println("Erste Gr��e");
    number1 = input.nextDouble();

    System.out.println("Zweite Gr��e");
    number2 = input.nextDouble();

    switch (operator) {

      case 'R':
        result = number1 / number2;
        System.out.println("R ist" + result);
        break;

      case 'U':
        result = number1 * number2;
        System.out.println("U ist" + result);
        break;

      
      case 'I':
        result = number1 / number2;
        System.out.println("I ist" + result);
        break;

      default:
        System.out.println("Falsche Eingabe!");
        break;
    }

    input.close();
  }
}