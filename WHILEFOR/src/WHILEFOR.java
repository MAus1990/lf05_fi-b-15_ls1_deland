public class WHILEFOR {
public static void main(String[] args) {
int summe;
int zahl;
// Summe der Zahlen von 1 bis 100 mit vorpr�fender Schleife:
summe = 0;
zahl = 1;
while (zahl <= 100) {
summe = summe + zahl;
zahl++;
System.out.println("while: " + zahl + " | " + summe);
}
// Summe der Zahlen von 1 bis 100 mit nachpr�fender Schleife:
summe = 0;
zahl = 1;
do {
summe = summe + zahl;
zahl++;
System.out.println("do while: " + zahl + " | " + summe);
} while (zahl <= 100);
// Summe der Zahlen von 1 bis 100 mit Z�hlschleife:
summe = 0;
for (zahl = 1; zahl <= 100; zahl++) {
summe = summe + zahl;
System.out.println("for: " + zahl + " | " + summe);
}
// Die folgende effizientere L�sung zeigt,
// dass Schleifen nicht immer die beste L�sung sind
summe = 101 * 50; // Formel: Summe (1 bis N) = (N +1) * N/2
System.out.println("Gau�sche Summenformel: " + summe);
}
}
