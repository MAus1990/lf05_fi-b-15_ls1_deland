import java.util.Scanner;

public class Taschenrechner {

	public static void main(String[] args) {
		double zahl1, zahl2, ergebnis;
		Scanner scannerVariable = new Scanner(System.in);
		System.out.println("Erste Zahl bitte.");
		zahl1=scannerVariable.nextDouble();
		System.out.println("Zweite Zahl bitte.");
		zahl2=scannerVariable.nextDouble();
		System.out.println("Geben Sie den Operanden ein");
		char Operand = scannerVariable.next().charAt(0);
		if(Operand == '+')
		{
			ergebnis=zahl1+zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (Operand == '-')
		{
			ergebnis=zahl1-zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (Operand == '*')
		{
			ergebnis=zahl1*zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else if (Operand == '/')
		{
			ergebnis=zahl1/zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
		}
		else 
		{
			System.out.println("Die Eingabe war falsch ");
		}
	}

}