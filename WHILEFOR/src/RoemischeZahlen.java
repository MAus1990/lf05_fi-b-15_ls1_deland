import java.util.Scanner;

	
public class RoemischeZahlen {
	static Scanner tastatur = new Scanner(System.in);
	public static void main(String[] args) {
		// Erstellen Sie die Konsolenanwendung Rom. Das Programm Rom soll nach der Eingabe
		//eines r�mischen Zahlzeichens die entsprechende Dezimalzahl ausgeben (I = 1, V = 5, X =
				//10, L = 50, C = 100, D = 500, M = 1000). Falls ein anderes Zeichen eingegeben wird, soll ein
				//entsprechender Hinweis ausgegeben werden.
		char RomZahl = tastatur.next().charAt(0);
		
		if(RomZahl == 'I')
		{
			System.out.println("1");
		}
		else if(RomZahl == 'V')
		{
			System.out.print("5");
		}
	
		else if(RomZahl == 'X')
		{
			System.out.print("10");
		}
		else if(RomZahl == 'L')
		{
			System.out.print("50");
		}
		else if(RomZahl == 'C')
		{
			System.out.print("100");
		}
		else if(RomZahl == 'D')
		{
			System.out.print("500");
		}
		else if(RomZahl == 'M')
		{
			System.out.print("1000");
		}
	}

}
