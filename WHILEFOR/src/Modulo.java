import java.util.Scanner;

public class Modulo {

	public static void main(String[] args) {



		// Aufgabe 2: Summe
		// Geben Sie in der Konsole die Summe der Zahlenfolgen aus. Erm�glichen Sie es
		// dem
		// Benutzer die Zahl x festzulegen, welche die Summierung begrenzt.
		// a) 1 + 2 + 3 + 4 +�+ x for-Schleife / while-Schleife
		// b) 2 + 4 + 6 +�+ 2x for-Schleife / while-Schleife
		// c) 1 + 3 + 5 +�+ (2x+1) for-Schleife / while-Schleife
		
		Scanner sumAScanner = new Scanner(System.in);
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein:");
		int x = sumAScanner.nextInt();
		
		int zahlA = 1;
		int zahlB = 2;
		int zahlC = 1;
		int summeA = 0;
		int summeB = 0;
		int summeC = 0; 
		
		// a: 
		
		do {
			summeA = summeA + zahlA;
			zahlA++;
		} while(zahlA <= x);
		
		System.out.println("Die Summe f�r A betr�gt:" + summeA);
		
		// b:
		
		do {
			summeB = summeB + zahlB;
			zahlB = zahlB + 2;
		} while(zahlB <= x * 2);
		
		System.out.println("Die Summe f�r B betr�gt:" + summeB);
	
		// c:
		do {
		summeC = summeC + zahlC;
		zahlC = zahlC + 2;
		} while(zahlC <= (2 * x) + 1);
	
		System.out.println("Die Summe f�r C betr�gt:" + summeC);
			
		}
}