
public class Temperatur {

	public static void main(String[] args) {

		System.out.printf("\nAufgabe 3\n");
		String fa = "Fahrenheit";
		String c = "Celsius";
		String a = "-----------------------";
		System.out.printf("\n%-12s|%10s",fa, c);
		System.out.printf("\n%s" ,a);
		System.out.printf("\n%-12d|%10.2f",-20,-28.8889);
		System.out.printf("\n%-12d|%10.2f",-10,-23.3333);
		System.out.printf("\n%+-12d|%10.2f",-0,-17.7778);
		System.out.printf("\n%+-12d|%10.2f",20,-6.6667);
		System.out.printf("\n%+-12d|%10.2f\n",30,-1.1111);

	}

}
