public class Volumenberechnung {
	
	public static void main(String[] args) {
		
		wurfel(10);
		quadrat(10,3,1);
		pyramide(8,10);
		kugel(4);
	}
	
	
	public static double wurfel(double a_cm) {
		double resultat = a_cm*a_cm*a_cm;
		System.out.printf("ein w�rfel mit eine seite von %.2f hat ein volum von %f\n",a_cm,resultat);
		return resultat;
	}
	public static double quadrat(double a_cm,double b_cm,double c_cm) {
		double resultat = a_cm*b_cm*c_cm;
		System.out.printf("ein quadrat mit einer h�he von %.2f,breite von %.2f und tiefe von %.2f  von  %f\n",a_cm,b_cm,c_cm,resultat);
		return resultat;
	}
	public static double pyramide(double a,double h) {
		double resultat = a*a*(h/3);
		System.out.printf("eine pyramide mit eine seite von %.2f und eine h�he von %.2f hat ein volumen von %f\n",a,h,resultat);
		return resultat;
	}
	public static double kugel(double r) {
		double resultat = 4/3*r*r*r*3.14;
		System.out.printf("eine kugel mit eine seite von %.2f hat ein volumen von %f\n",r,resultat);
		return resultat;
	}
}