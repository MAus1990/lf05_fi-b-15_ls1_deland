
public class Artikel {

	private String name;
	private int artikelnr;
	private double einkaufspreis;
	private double verkaufspreis;
	private int mindestbestand;
	private int lagerbestand;

	public Artikel()
	{
		
	}
	public Artikel(String name)
	{
		this.name= name;
	}
	public Artikel(String name, int artikelnr, double einkaufspreis, double verkaufspreis, int mindestbestand, int lagerbestand)
	{
		this.name = name;
		this.artikelnr= artikelnr;
		this.einkaufspreis=einkaufspreis;
		this.verkaufspreis= verkaufspreis;
		this.mindestbestand=mindestbestand;
		this.lagerbestand= lagerbestand;
		
	}
	
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public void setArtikelnr(int artikelnr) {
		this.artikelnr = artikelnr;
	}

	public double getArtikelnr() {
		return this.artikelnr;
	}

	public void setEinkaufspreis(double einkaufspreis) {
		this.einkaufspreis = einkaufspreis;
	}

	public double getEinkaufspreis() {
		return this.einkaufspreis;
	}

	public void setVerkaufspreis(double verkaufspreis) {
		this.verkaufspreis = verkaufspreis;
	}

	public double getVerkaufspreis() {
		return this.verkaufspreis;
	}

	public void setMindestbestand(int mindestbestand) {
		this.mindestbestand = mindestbestand;
	}

	public double getMindestbestand() {
		return this.mindestbestand;
	}

	public void setLagerbestand(int lagerbestand) {
		this.lagerbestand = lagerbestand;
	}

	public double getLagerbestand() {
		return this.lagerbestand;
	}

	public void bestelleArtikel() {
		if(this.lagerbestand<(this.mindestbestand*0.80))
		{
			 this.lagerbestand = this.mindestbestand;
		}
	}

	public void changeLagerbestand() {
	}

	public void gewinnMarge() 
	{	 
		
	}

}
