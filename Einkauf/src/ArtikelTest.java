
public class ArtikelTest {

	public static void main(String[] args) 
	{
		 Artikel art1 = new Artikel();
		 Artikel art2 = new Artikel();
		 Artikel art3= new Artikel("EIS",2,0.50,1.50,150,450);

		 //Setzen der Attribute
		 art1.setName("Himalayareis");
		 art1.setEinkaufspreis(1.41);
		 art1.setVerkaufspreis(1.78);
		 art1.setArtikelnr(5);
		 art1.setMindestbestand(100);	
		 art1.setLagerbestand(412);
		 
		 art2.setName("Frische Fische von Fischers Fritze");
		 art2.setEinkaufspreis(7.21);
		 art2.setVerkaufspreis(12.45);
		 art2.setMindestbestand(10);
		 art2.setArtikelnr(581);
		 art2.setLagerbestand(3);
		 //Bildschirmausgaben
		 System.out.println("Name: " + art1.getName());
		 System.out.println("Einkaufspreis: " + art1.getEinkaufspreis() + " Euro");
		 System.out.println("\nName: " + art2.getName());
		
		 System.out.println("Name: " + art3.getName());
		 System.out.println("Einkaufspreis: " + art3.getEinkaufspreis() + " Euro");
	
		// TODO Auto-generated method stub

	}

}
