import java.util.ArrayList;

public class Raumschiff {
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	private static ArrayList<String> broadcast;
	private static ArrayList<Ladung> ladungsverzeichnis;

	public Raumschiff() {

	}

	public Raumschiff(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int zustandSchildeInProzent,
			int zustandHuelleInProzent, int zustandLebenserhaltungssystemeInProzent, int anzahlDroiden,
			String schiffsname) {
		this(schiffsname);
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.androidenAnzahl = androidenAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = zustandSchildeInProzent;
		this.huelleInProzent = zustandHuelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;

	}

	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}

	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}

	public void photonentorpedoShoot(Raumschiff Ziel ) {
		if(this.photonentorpedoAnzahl==0)
		{ 
			System.out.print("KEINE MUNITION!");
			
		}
		else 
		{
			this.photonentorpedoAnzahl=-1;
			Ziel.treffer();
		}
	}

	public void phaserCannonShoot(Raumschiff Ziel)
	{	if(this.schildeInProzent>=0 && this.huelleInProzent>=0)	
		System.out.print("Ziel getroffen!");
		Ziel.treffer();
	}
	
	public void treffer()
	{
		if(this.schildeInProzent>=0)
		{
			this.schildeInProzent= - 50;	
		}
		if(this.schildeInProzent<=0) 
		{
			this.huelleInProzent=-50;
		}
		
	}
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}

	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}

	public int getSchildeInProzent() {
		return schildeInProzent;
	}

	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}

	public int getHuelleInProzent() {
		return huelleInProzent;
	}

	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}

	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}

	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}

	public String getSchiffsname() {
		return schiffsname;
	}

	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	public static ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}

	public static void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		Raumschiff.ladungsverzeichnis = ladungsverzeichnis;
	}

	public static ArrayList<String> eintragLogbuch() {
		return broadcast;
	}

//	public void addLadung(ArrayList<Ladung> ladungsverzeichnis ) 
//	{
//		ladungsverzeichnis.add();
//	}

	public void reparatur(boolean schutzschilde, boolean energieversorgung, boolean schiffshuelle, int anzahlDroiden) {
	}

	public void zustandRaumschiff() {

	}

}
